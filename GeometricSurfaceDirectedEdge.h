///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2018
//
//	------------------------
//	GeometricSurfaceDirectedEdge.h
//	------------------------
//	
//	Base code for geometric assignments.
//
//	This is the minimalistic Face-based D/S for storing
//	surfaces, to be used as the basis for fuller versions
//	
//	It will include object load / save code & render code
//	
///////////////////////////////////////////////////

#ifndef _GEOMETRIC_SURFACE_DIRECTED_EDGE_H
#define _GEOMETRIC_SURFACE_DIRECTED_EDGE_H

#include <vector>
#include <queue>
#include <iostream>

// an index type - signed so we can use negative numbers as flags
typedef signed long indexType;

// define a macro for "not used" flag
#define NO_SUCH_ELEMENT -1

#include "Cartesian3.h"

class GeometricSurfaceDirectedEdge
	{ // class GeometricSurfaceDirectedEdge
	public:

	struct diredgeMesh
	{
		// the vertex positions
		std::vector<Cartesian3> position;
		// the "first" directed edge for each vertex
		std::vector<indexType> firstDirectedEdge;
		// the face vertices - doubles as the "to" array for edges
		std::vector<indexType> faceVertices;
		// the other half of the directed edges
		std::vector<indexType> otherHalf;
		// array to hold the normals
		std::vector<Cartesian3> normal;
	};

	typedef struct ranking
	{
		int vertex_number;
		float value;
		int other_vertex;
		ranking(int vertex_number, float value, int other_vertex) : vertex_number(vertex_number), value(value), other_vertex(other_vertex)
    	{ 
    	} 
	};

	struct CompareValue { 
    	bool operator()(ranking r1, ranking r2) 
    	{  
        	if(r1.value < r2.value) return 0;
			if(r1.value > r2.value) return 1;
			if(&(r1.value) < &(r2.value)) return 0;
			if(&(r1.value) > &(r2.value)) return 1;
   	 	} 
	}; 
	
	diredgeMesh* Mesh;
	diredgeMesh* displayedMesh;

	std::priority_queue<ranking, std::vector<ranking>, CompareValue>* Q;

	// bounding sphere size
	float boundingSphereSize;

	// midpoint of object
	Cartesian3 midPoint;

	// constructor will initialise to safe values
	GeometricSurfaceDirectedEdge();
		
	// read routine returns true on success, failure otherwise
	// does *NOT* check consistency
	bool ReadFileDirEdge(char *fileName);

	// write routine
	bool WriteFileDirEdge(char *fileName, diredgeMesh mesh);
	
	// routine to render
	void Render(diredgeMesh mesh);
	
	// debug routine to dump arrays
	void DumpArrays();

	// Makes a half edge mesh data structure from a triangle soup.
    void createMesh(std::vector<Cartesian3>);

    // Makes a triangle soup from the half edge mesh data structure.
    std::vector<Cartesian3> makeSoup(diredgeMesh mesh);

	// Computes mesh.position and mesh.normal and mesh.faceVertices
    void makeFaceIndices(std::vector<Cartesian3> raw_vertices, diredgeMesh* mesh);

    // Computes mesh.firstDirectedEdge and mesh.firstDirectedEdge, given mesh.position and mesh.normal and mesh.faceVertices
    void makeDirectedEdges(diredgeMesh* mesh);

	void generatePriorityQueue(diredgeMesh mesh);

	ranking calculateValue(diredgeMesh mesh, int vertex_no);

	void collapseEdge(int v1, int v2);

	void collapseEdges(int scale);
}; // class GeometricSurfaceDirectedEdge
#endif