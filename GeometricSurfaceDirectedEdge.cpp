///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2018
//
//	--------------------------------
//	GeometricSurfaceDirectedEdge.cpp
//	--------------------------------
//
//	Fully implemented Directed Edge code
//
///////////////////////////////////////////////////

#include "GeometricSurfaceDirectedEdge.h"
#include <stdio.h>
#include <math.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

// use macros for the "previous" and "next" IDs
#define PREVIOUS_EDGE(x) ((x) % 3) ? ((x) - 1) : ((x) + 2)
#define NEXT_EDGE(x) (((x) % 3) == 2) ? ((x) - 2) : ((x) + 1)

// constructor will initialise to safe values
GeometricSurfaceDirectedEdge::GeometricSurfaceDirectedEdge()
{ // GeometricSurfaceDirectedEdge::GeometricSurfaceDirectedEdge()
	// force the size to nil (should not be necessary, but . . .)
	Mesh = new diredgeMesh();
	Mesh->position.resize(0);
	Mesh->firstDirectedEdge.resize(0);
	Mesh->faceVertices.resize(0);
	Mesh->otherHalf.resize(0);
	Mesh->normal.resize(0);
	
	displayedMesh = new diredgeMesh();
	displayedMesh->position.resize(0);
	displayedMesh->firstDirectedEdge.resize(0);
	displayedMesh->faceVertices.resize(0);
	displayedMesh->otherHalf.resize(0);
	displayedMesh->normal.resize(0);
	// set this to something reasonable
	boundingSphereSize = 1.0;
	// set the midpoint to the origin
	midPoint = Cartesian3(0.0, 0.0, 0.0);

} // GeometricSurfaceDirectedEdge::GeometricSurfaceDirectedEdge()

// read routine returns true on success, failure otherwise
bool GeometricSurfaceDirectedEdge::ReadFileDirEdge(char *fileName)
{ // GeometricSurfaceDirectedEdge::ReadFileDirEdge()
	// these are for accumulating a bounding box for the object
	Cartesian3 minCoords(1000000.0, 1000000.0, 1000000.0);
	Cartesian3 maxCoords(-1000000.0, -1000000.0, -1000000.0);

	// open the input file
	FILE *inFile = fopen(fileName, "r");
	if (inFile == NULL)
		return false;

	// set the number of vertices and faceVertices
	indexType nVertices = 0, nFaces = 0;

	// set the midpoint to the origin
	midPoint = Cartesian3(0.0, 0.0, 0.0);

	// assume that file format is ABSOLUTELY predictable and the lines aren't too long
	char lineBuffer[256];

	// the first four lines will be skipped completely
	fgets(lineBuffer, 256, inFile);
	fgets(lineBuffer, 256, inFile);
	fgets(lineBuffer, 256, inFile);
	fgets(lineBuffer, 256, inFile);

	// read the only header line we care about
	fscanf(inFile, "# %s vertices=%ld faces=%ld \n", lineBuffer, &nVertices, &nFaces);

	// the next line is skipped
	fgets(lineBuffer, 256, inFile);

	// now allocate space for them all
	Mesh->position.resize(nVertices);
	Mesh->normal.resize(nVertices);
	Mesh->firstDirectedEdge.resize(nVertices);
	Mesh->faceVertices.resize(nFaces*3);
	Mesh->otherHalf.resize(nFaces*3);	

	// now loop to read the vertices in, and hope nothing goes wrong
	for (indexType vertex = 0; vertex < nVertices; vertex++)
		{ // for each vertex
		indexType vertexID;
		// scan in the vertex ID
		indexType nScanned = fscanf(inFile, "Vertex %ld", &vertexID);
		// if it didn't scan, or was the wrong ID, exit
		if ((nScanned != 1) || (vertexID != vertex))
			{ // scan failed
			printf("Invalid vertex %ld\n", vertex);
			return false;
			} // scan failed
		// now try to read in the vertex (failure => non-zero)
		if (3 != fscanf(inFile, "%f %f %f ", &(Mesh->position[vertex].x), &(Mesh->position[vertex].y), &(Mesh->position[vertex].z)))
			{ // scan failed
			printf("Invalid vertex %ld\n", vertex);
			return false;
			} // scan failed

		// keep running track of midpoint, &c.
		midPoint = midPoint + Mesh->position[vertex];
		if (Mesh->position[vertex].x < minCoords.x) minCoords.x = Mesh->position[vertex].x;
		if (Mesh->position[vertex].y < minCoords.y) minCoords.y = Mesh->position[vertex].y;
		if (Mesh->position[vertex].z < minCoords.z) minCoords.z = Mesh->position[vertex].z;

		if (Mesh->position[vertex].x > maxCoords.x) maxCoords.x = Mesh->position[vertex].x;
		if (Mesh->position[vertex].y > maxCoords.y) maxCoords.y = Mesh->position[vertex].y;
		if (Mesh->position[vertex].z > maxCoords.z) maxCoords.z = Mesh->position[vertex].z;
		} // for each vertex

	// now sort out the size of a bounding sphere for viewing
	// and also set the midpoint's location
	midPoint = midPoint / Mesh->position.size();

	// now go back through the vertices, subtracting the mid point
	for (int vertex = 0; vertex < nVertices; vertex++)
		{ // per vertex
		Mesh->position[vertex] = Mesh->position[vertex] - midPoint;
		} // per vertex

	// the bounding sphere radius is just half the distance between these
	boundingSphereSize = sqrt((maxCoords - minCoords).length()) * 0.5;

	// now we read in the vertex normal vectors
	for (indexType vertex = 0; vertex < nVertices; vertex++)
		{ // for each vertex
		indexType vertexID;
		// scan in the vertex ID
		indexType nScanned = fscanf(inFile, "Normal %ld", &vertexID);
		// if it didn't scan, or was the wrong ID, exit
		if ((nScanned != 1) || (vertexID != vertex))
			{ // scan failed
			printf("Invalid vertex ID %ld\n", vertex);
			return false;
			} // scan failed
		// now try to read in the vertex (failure => non-zero)
		if (3 != fscanf(inFile, "%f %f %f ", &(Mesh->normal[vertex].x), &(Mesh->normal[vertex].y), &(Mesh->normal[vertex].z)))
			{ // scan failed
			printf("Invalid vertex normal %ld\n", vertex);
			return false;
			} // scan failed
		} // for each vertex

	// next, we read in the first directed edge array
	for (indexType vertex = 0; vertex < nVertices; vertex++)
		{ // for each vertex
		indexType vertexID;
		// scan in the vertex ID
		indexType nScanned = fscanf(inFile, "FirstDirectedEdge %ld", &vertexID);
		// if it didn't scan, or was the wrong ID, exit
		if ((nScanned != 1) || (vertexID != vertex))
			{ // scan failed
			printf("Invalid vertex ID for first directed edge %ld %ld\n", vertexID, vertex);
			return false;
			} // scan failed
		// now try to read in the first directed edge (failure => non-zero)
		if (1 != fscanf(inFile, "%ld ", &(Mesh->firstDirectedEdge[vertex])))
			{ // scan failed
			printf("Invalid first directed edge %ld for vertex %ld\n", Mesh->firstDirectedEdge[vertex], vertex);
			return false;
			} // scan failed
		} // for each vertex

	// similarly, loop to read the faceVertices in
	for (indexType face = 0; face < nFaces; face++)
		{ // for each face
		indexType faceID;

		// scan in the vertex ID
		indexType nScanned = fscanf(inFile, "Face %ld %ld %ld %ld ",
			&faceID, &(Mesh->faceVertices[3*face]), &(Mesh->faceVertices[3*face+1]), &(Mesh->faceVertices[3*face+2]));

		// if it didn't scan, or was the wrong ID, exit
		if ((nScanned != 4) || (faceID != face))
			{ // scan failed
			printf("Invalid face %ld\n", face);
			return false;
			} // scan failed
		} // for each face

	// repeat for other half
	for (indexType dirEdge = 0; dirEdge < nFaces * 3; dirEdge++)
		{ // for each directed edge
		indexType dirEdgeID;

		// scan in the vertex ID
		indexType nScanned = fscanf(inFile, "OtherHalf %ld %ld ", &dirEdgeID, &(Mesh->otherHalf[dirEdge]));

		// if it didn't scan, or was the wrong ID, exit
		if ((nScanned != 2) || (dirEdgeID != dirEdge))
			{ // scan failed
			printf("Invalid directed edge ID %ld %ld\n", dirEdgeID, dirEdge);
			return false;
			} // scan failed
		} // for each directed edge
	displayedMesh->faceVertices = Mesh->faceVertices;
	displayedMesh->firstDirectedEdge = Mesh->firstDirectedEdge;
	displayedMesh->normal = Mesh->normal;
	displayedMesh->otherHalf = Mesh->otherHalf;
	displayedMesh->position = Mesh->position;
	return true;
} // GeometricSurfaceDirectedEdge::ReadFileDirEdge()

// write routine
bool GeometricSurfaceDirectedEdge::WriteFileDirEdge(char *fileName, diredgeMesh mesh)
{ // GeometricSurfaceDirectedEdge::WriteFileDirEdge()
	// open the input file
	FILE *outFile = fopen(fileName, "w");
	if (outFile == NULL)
		return false;

	// the first four lines will be skipped completely
	fprintf(outFile, "#\n");
	fprintf(outFile, "# Created for Leeds COMP 5821M Spring 2018\n");
	fprintf(outFile, "#\n");
	fprintf(outFile, "#\n");
	fprintf(outFile, "# Surface vertices=%ld faces=%ld\n", mesh.position.size(), mesh.faceVertices.size()/3);
	fprintf(outFile, "#\n");

	// loop to write the vertices out
	for (indexType vertex = 0; vertex < (indexType) mesh.position.size(); vertex++)
 		fprintf(outFile, "Vertex %1ld %9.6f %9.6f %9.6f\n", vertex, mesh.position[vertex].x, mesh.position[vertex].y, mesh.position[vertex].z);

	// loop to write out the normals
	for (indexType vertex = 0; vertex < (indexType) mesh.position.size(); vertex++)
 		fprintf(outFile, "Normal %1ld %9.6f %9.6f %9.6f\n", vertex, mesh.normal[vertex].x, mesh.normal[vertex].y,mesh. normal[vertex].z);

	// loop to write out the first directed edge index
	for (indexType vertex = 0; vertex < (indexType) mesh.position.size(); vertex++)
		fprintf(outFile, "FirstDirectedEdge %1ld  %2ld\n", vertex, mesh.firstDirectedEdge[vertex]);

	// loop to write the face vertices out
	for (indexType face = 0; face < (indexType) mesh.faceVertices.size()/3; face++)
		fprintf(outFile, "Face %2ld  %2ld %2ld %2ld\n", face, mesh.faceVertices[3*face], mesh.faceVertices[3*face+1], mesh.faceVertices[3*face+2]);

	// loop to write out the edge pairs
	for (indexType dirEdge = 0; dirEdge < (indexType) mesh.faceVertices.size(); dirEdge++)
		fprintf(outFile, "Edge %1ld  %2ld\n", dirEdge, mesh.otherHalf[dirEdge]);

	// close the file & return
	fclose(outFile);
	return true;
} // GeometricSurfaceDirectedEdge::WriteFileDirEdge()

// routine to render
void GeometricSurfaceDirectedEdge::Render(diredgeMesh mesh)
{ // GeometricSurfaceDirectedEdge::Render()
// for(int i = 0; i < displayedMesh->faceVertices.size() / 3; i++){
// 		std::cout << displayedMesh->faceVertices[i*3 + 0] << " ";
// 		std::cout << displayedMesh->faceVertices[i*3 + 1] << " ";
// 		std::cout << displayedMesh->faceVertices[i*3 + 2] << std::endl;
// 	}
	glBegin(GL_TRIANGLES);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	// since normals pair with vertices, loop is simpler
	for (unsigned int face = 0; face < mesh.faceVertices.size(); face ++)
		{ // per face
		Cartesian3 *v0 = &(mesh.position[mesh.faceVertices[face]]);
		Cartesian3 *n0 = &(mesh.normal[mesh.faceVertices[face]]);

		glNormal3fv((GLfloat *) n0);
		glVertex3fv((GLfloat *) v0);
		} // per face
	glEnd();
} // GeometricSurfaceDirectedEdge::Render()

// debug routine to dump arrays
void GeometricSurfaceDirectedEdge::DumpArrays()
{ // GeometricSurfaceDirectedEdge::DumpArrays()
	printf("\n");
	printf("-------------------------------------------------------------------\n");
	printf("Dumping Arrays\n");
	// print a header line
	printf("Vertex ID:    |");
	for (indexType vertex = 0; vertex < (indexType) Mesh->position.size(); vertex++)
		printf("%9ld ", vertex);
	printf("\n");
	printf("--------------+");
	for (indexType vertex = 0; vertex < (indexType) Mesh->position.size(); vertex++)
		printf("----------");
	printf("\n");
	// print the data
	printf("Position x    |");
	for (indexType vertex = 0; vertex < (indexType) Mesh->position.size(); vertex++)
		printf("%9.5f ", Mesh->position[vertex].x);
	printf("\n");
	printf("Position y    |");
	for (indexType vertex = 0; vertex < (indexType) Mesh->position.size(); vertex++)
		printf("%9.5f ", Mesh->position[vertex].y);
	printf("\n");
	printf("Position z    |");
	for (indexType vertex = 0; vertex < (indexType) Mesh->position.size(); vertex++)
		printf("%9.5f ", Mesh->position[vertex].z);
	printf("\n");
	printf("Normal x      |");
	for (indexType vertex = 0; vertex < (indexType) Mesh->position.size(); vertex++)
		printf("%9.5f ", Mesh->normal[vertex].x);
	printf("\n");
	printf("Normal y      |");
	for (indexType vertex = 0; vertex < (indexType) Mesh->position.size(); vertex++)
		printf("%9.5f ", Mesh->normal[vertex].y);
	printf("\n");
	printf("Normal z      |");
	for (indexType vertex = 0; vertex < (indexType) Mesh->position.size(); vertex++)
		printf("%9.5f ", Mesh->normal[vertex].z);
	printf("\n");
	printf("First DirEdge |");
	for (indexType vertex = 0; vertex < (indexType) Mesh->position.size(); vertex++)
		printf("%9ld ", Mesh->firstDirectedEdge[vertex]);
	printf("\n");
	// print a header line
	printf("Vertex ID:    |");
	for (indexType dirEdge = 0; dirEdge < (indexType) Mesh->faceVertices.size(); dirEdge++)
		printf("%9ld ", dirEdge);
	printf("\n");
	printf("--------------+");
	for (indexType dirEdge = 0; dirEdge < (indexType) Mesh->faceVertices.size(); dirEdge++)
		printf("----------");
	printf("\n");
	printf("Face Vertices |");
	for (indexType dirEdge = 0; dirEdge < (indexType) Mesh->faceVertices.size(); dirEdge++)
		printf("%9ld ", Mesh->faceVertices[dirEdge]);
	printf("\n");
	printf("Other Half    |");
	for (indexType dirEdge = 0; dirEdge < (indexType) Mesh->faceVertices.size(); dirEdge++)
		printf("%9ld ", Mesh->otherHalf[dirEdge]);
	printf("\n");
	printf("-------------------------------------------------------------------\n");
	printf("\n");
} // GeometricSurfaceDirectedEdge::DumpArrays()

std::vector<Cartesian3> GeometricSurfaceDirectedEdge::makeSoup(diredgeMesh mesh)
{
    std::vector<Cartesian3> soup;
    for (long face = 0; face < (long) mesh.faceVertices.size()/3; face++)
    {
        for (int i = 0 ; i < 3 ; i++)
        {
            Cartesian3 position = mesh.position[mesh.faceVertices[3*face + i]];
            soup.push_back(position);
        }
    }
    return soup;
}

void GeometricSurfaceDirectedEdge::createMesh(std::vector<Cartesian3> raw_vertices)
{
    Mesh->faceVertices.resize(raw_vertices.size(), -1);
    Mesh->normal.resize(raw_vertices.size()/3, Cartesian3(0.0, 0.0, 0.0));
    makeFaceIndices(raw_vertices, Mesh);

    Mesh->otherHalf.resize(Mesh->faceVertices.size(), NO_SUCH_ELEMENT);
    Mesh->firstDirectedEdge.resize(Mesh->position.size(), NO_SUCH_ELEMENT);
    makeDirectedEdges(Mesh);
	displayedMesh = Mesh;
}

void GeometricSurfaceDirectedEdge::makeFaceIndices(std::vector<Cartesian3> raw_vertices, diredgeMesh* mesh)
{
    // set the initial vertex ID
    long nextVertexID = 0;

    // loop through the vertices
    for (unsigned long vertex = 0; vertex < raw_vertices.size(); vertex++)
    { // vertex loop
        // first see if the vertex already exists
        for (unsigned long other = 0; other < vertex; other++)
        { // per other
            if (raw_vertices[vertex] == raw_vertices[other])
                mesh->faceVertices[vertex] = mesh->faceVertices[other];
        } // per other
        // if not set, set to next available
        if (mesh->faceVertices[vertex] == -1)
            mesh->faceVertices[vertex] = nextVertexID++;
    } // vertex loop

    // id of next vertex to write
    long writeID = 0;

    for (long vertex = 0; vertex < raw_vertices.size(); vertex++)
    { 
        // if it's the first time found
        if (writeID == mesh->faceVertices[vertex])
        { 
            mesh->position.push_back(raw_vertices[vertex]);
            writeID++;
        }
    }
	// for(int i = 0; i < mesh->faceVertices.size() / 3; i++){
	// 	std::cout << mesh->faceVertices[i*3 + 0] << " ";
	// 	std::cout << mesh->faceVertices[i*3 + 1] << " ";
	// 	std::cout << mesh->faceVertices[i*3 + 2] << std::endl;
	// }
}

void GeometricSurfaceDirectedEdge::makeDirectedEdges(diredgeMesh* mesh)
{
    // we will also want a temporary variable for the degree of each vertex
    std::vector<long> vertexDegree(mesh->position.size(), 0);

    // 2.	now loop through the directed edges
    for (long dirEdge = 0; dirEdge < (long) mesh->faceVertices.size(); dirEdge++)
    { // for each directed edge
        // a. retrieve to and from vertices
        long from = mesh->faceVertices[dirEdge];
        long to = mesh->faceVertices[NEXT_EDGE(dirEdge)];

        // aa. Error check for duplicated vertices on faces
        if (from == to)
        { // error: duplicate vertex on face
            printf("Error: Directed Edge %ld has matching ends %ld %ld\n", dirEdge, from, to);
            exit(0);
        } // error: duplicate vertex on face

        // b. if from vertex has no "first", set it
        if (mesh->firstDirectedEdge[from] == NO_SUCH_ELEMENT)
            mesh->firstDirectedEdge[from] = dirEdge;

        // increment the vertex degree
        vertexDegree[from]++;

        // c. if the other half is already set, we can skip this edge
        if (mesh->otherHalf[dirEdge] != NO_SUCH_ELEMENT)
            continue;

        // d. set a counter for how many matching edges
        long nMatches = 0;

        // e. now search all directed edges on higher index faces
        long face = dirEdge / 3;
        for (long otherEdge = 3 * (face + 1); otherEdge < (long) mesh->faceVertices.size(); otherEdge++)
        { // for each higher face edge
            // i. retrieve other's to and from
            long otherFrom = mesh->faceVertices[otherEdge];
            long otherTo = mesh->faceVertices[NEXT_EDGE(otherEdge)];

            // ii. test for match
            if ((from == otherTo) && (to == otherFrom))
            { // match
                // if it's not the first match, we have a non-manifold edge
                if (nMatches > 0)
                { // non-manifold edge
                    printf("Error: Directed Edge %ld matched more than one other edge (%ld, %ld)\n", dirEdge, mesh->otherHalf[dirEdge], otherEdge);
                    exit(0);
                } // non-manifold edge

                // otherwise we set the two edges to point to each other
                mesh->otherHalf[dirEdge] = otherEdge;
                mesh->otherHalf[otherEdge] = dirEdge;

                // increment the counter
                nMatches ++;
            } // match

        } // for each higher face edge

        // f. if it falls through here with no matches, we know it is non-manifold
        if (nMatches == 0)
        { // non-manifold edge
            printf("Error: Directed Edge %ld (%ld, %ld) matched no other edge\n", dirEdge, from, to);
            exit(0);
        } // non-manifold edge

    } // for each other directed edge

	// for(int i = 0; i < mesh->faceVertices.size() / 3; i++){
	// 	std::cout << mesh->faceVertices[i*3 + 0] << " ";
	// 	std::cout << mesh->faceVertices[i*3 + 1] << " ";
	// 	std::cout << mesh->faceVertices[i*3 + 2] << std::endl;
	// }


    // 3.	now we assume that the data structure is correctly set, and test whether all neighbours are on a single cycle
    for (long vertex = 0; vertex < (long) mesh->position.size(); vertex++)
    { // for each vertex
        // start a counter for cycle length
        long cycleLength = 0;

        // loop control is the neighbouring edge
        long outEdge = mesh->firstDirectedEdge[vertex];

        // could happen in a malformed input
        if (outEdge == NO_SUCH_ELEMENT)
        { // no first edge
            printf("Error: Vertex %ld had not incident edges\n", vertex);
        } // no first edge

        // do loop to iterate correctly
        do
        { // do loop
            // while we are at it, we can set the normal
            long face = outEdge / 3;
			//std::cout << face << std::endl;
            Cartesian3 *v0 = &(mesh->position[mesh->faceVertices[3*face]]);
            Cartesian3 *v1 = &(mesh->position[mesh->faceVertices[3*face+1]]);
            Cartesian3 *v2 = &(mesh->position[mesh->faceVertices[3*face+2]]);
            // now compute the normal vector
            Cartesian3 uVec = *v2 - *v0;
            Cartesian3 vVec = *v1 - *v0;
            Cartesian3 faceNormal = uVec.cross(vVec);
            mesh->normal[vertex] = mesh->normal[vertex] + faceNormal;

            // flip to the other half
            long edgeFlip = mesh->otherHalf[outEdge];
            // take the next edge on its face
            outEdge = NEXT_EDGE(edgeFlip);
            // increment the cycle length
            cycleLength ++;

        } // do loop
        while (outEdge != mesh->firstDirectedEdge[vertex]);

        // now check the length against the vertex degree
        if (cycleLength != vertexDegree[vertex])
        { // wrong cycle length
            printf("Error: vertex %ld has edge cycle of length %ld but degree of %ld\n", vertex, cycleLength, vertexDegree[vertex]);
            exit(0);
        } // wrong cycle length

        // normalise the vertex normal
        mesh->normal[vertex] = mesh->normal[vertex].normalise();
    } // for each vertex

}

void GeometricSurfaceDirectedEdge::generatePriorityQueue(diredgeMesh mesh)
{
	//Q = std::priority_queue<ranking, std::vector<ranking>, CompareValue>();
	while(!Q->empty()){
		std::cout << Q->top().value <<std::endl;
		Q->pop();
	}
	for(int i = 0; i < mesh.position.size(); i++)
	{
		ranking rank = calculateValue(*displayedMesh, i);
		if(rank.value != -1){
			Q->push(rank);
		}
	}
}

GeometricSurfaceDirectedEdge::ranking GeometricSurfaceDirectedEdge::calculateValue(diredgeMesh mesh, int vertex_no)
{
	ranking rank(0,-1,0);
	float shortest_dist = 9999;
	float total_area = 0;
	int degree = 0;
	//rank.vertex_number = vertex_no;
	for(int i = 0; i < mesh.faceVertices.size(); i++){
		
	}


	for(int i = 0; i < mesh.faceVertices.size()/3; i++){
		if(vertex_no == mesh.faceVertices[i*3 + 0]){
			Cartesian3 v1 = mesh.position[mesh.faceVertices[i*3 + 1]] - mesh.position[vertex_no];
			Cartesian3 v2 = mesh.position[mesh.faceVertices[i*3 + 2]] - mesh.position[vertex_no];
			Cartesian3 v1xv2 = v1.cross(v2);
			float area = 0.5 * v1xv2.length();
			total_area += area;
			float dist = v1.length();
			if(dist < shortest_dist){
				shortest_dist = dist;
				rank.other_vertex = mesh.faceVertices[i*3 + 1];
			}
			degree++;
		}
		else if(vertex_no == mesh.faceVertices[i*3 + 1]){
			Cartesian3 v1 = mesh.position[mesh.faceVertices[i*3 + 2]] - mesh.position[vertex_no];
			Cartesian3 v2 = mesh.position[mesh.faceVertices[i*3 + 0]] - mesh.position[vertex_no];
			Cartesian3 v1xv2 = v1.cross(v2);
			float area = 0.5 * v1xv2.length();
			total_area += area;
			float dist = v1.length();
			if(dist < shortest_dist){
				shortest_dist = dist;
				rank.other_vertex = mesh.faceVertices[i*3 + 2];
			}
			degree++;
		}
		else if(vertex_no == mesh.faceVertices[i*3 + 2]){
			Cartesian3 v1 = mesh.position[mesh.faceVertices[i*3 + 0]] - mesh.position[vertex_no];
			Cartesian3 v2 = mesh.position[mesh.faceVertices[i*3 + 1]] - mesh.position[vertex_no];
			Cartesian3 v1xv2 = v1.cross(v2);
			float area = 0.5 * v1xv2.length();
			total_area += area;
			float dist = v1.length();
			if(dist < shortest_dist){
				shortest_dist = dist;
				rank.other_vertex = mesh.faceVertices[i*3 + 0];
			}
			degree++;
		}
	}
	rank.value = total_area;
	//rank.value = degree;
	rank.vertex_number = vertex_no;
	return rank;
}

void GeometricSurfaceDirectedEdge::collapseEdges(int scale)
{
	displayedMesh->faceVertices = Mesh->faceVertices;
	displayedMesh->firstDirectedEdge = Mesh->firstDirectedEdge;
	displayedMesh->normal = Mesh->normal;
	displayedMesh->otherHalf = Mesh->otherHalf;
	displayedMesh->position = Mesh->position;
	float scale_norm = (float)scale/100.0f;
	int counter = 0;
	
	while(displayedMesh->faceVertices.size()/3 > ((Mesh->faceVertices.size()/3)*(1.0f-scale_norm)))
	//for(int i = 0; i < 10; i++)
	{
		std::cout<< "done "<< counter << " of: " << (displayedMesh->faceVertices.size()/3 > ((Mesh->faceVertices.size()/3)*(1.0f-scale_norm))) << std::endl;
		generatePriorityQueue(*displayedMesh);
		//Q->pop();
		ranking edge = Q->top();
		while(edge.value == 0){
			Q->pop();
			edge = Q->top();
		}
		std::cout << "v1: " << edge.vertex_number << " v2: " << edge.other_vertex << " value: " << edge.value << std::endl;
		Q->pop();
		collapseEdge(edge.vertex_number, edge.other_vertex);
	}
}

void GeometricSurfaceDirectedEdge::collapseEdge(int v1, int v2)
{
	
	Cartesian3 new_position = (displayedMesh->position[v1] + displayedMesh->position[v2]) / 2.0f;
	Cartesian3 new_normal = (displayedMesh->normal[v1] + displayedMesh->normal[v2]) / 2.0f;
	std::cout << new_position.x << " " << new_position.y << " " << new_position.z << " " << std::endl;
	//displayedMesh->position.push_back(new_position);
	//int new_index = displayedMesh->position.size() - 1;
	int f1 = -1,f2 = -1;
	for (int i = 0; i < displayedMesh->faceVertices.size()/3; i++)
	{
		int vert0 = displayedMesh->faceVertices[i*3 + 0];
		int vert1 = displayedMesh->faceVertices[i*3 + 1];
		int vert2 = displayedMesh->faceVertices[i*3 + 2];

		if((v1 == vert0 || v1 == vert1 || v1 == vert2) && (v2 == vert0 || v2 == vert1 || v2 == vert2)){
			if(f1 == -1){
				f1 = 3*i;
			}
			else {
				f2 = 3*i;
			}
		}
		
	}
	if(f1 > f2){
		displayedMesh->faceVertices.erase(displayedMesh->faceVertices.begin()+f1,displayedMesh->faceVertices.begin()+f1+3);
		displayedMesh->faceVertices.erase(displayedMesh->faceVertices.begin()+f2,displayedMesh->faceVertices.begin()+f2+3);
		//displayedMesh->position.erase(displayedMesh->position.begin()+(f1/3));
		//displayedMesh->position.erase(displayedMesh->position.begin()+(f2/3));
	}
	else
	{
		displayedMesh->faceVertices.erase(displayedMesh->faceVertices.begin()+f2,displayedMesh->faceVertices.begin()+f2+3);
		displayedMesh->faceVertices.erase(displayedMesh->faceVertices.begin()+f1,displayedMesh->faceVertices.begin()+f1+3);
		//displayedMesh->position.erase(displayedMesh->position.begin()+(f2/3));
		//displayedMesh->position.erase(displayedMesh->position.begin()+(f1/3));
	}

	displayedMesh->position[v1] = new_position;
	displayedMesh->normal[v1] = new_normal;
	
	for(int i = 0; i < displayedMesh->faceVertices.size(); i++)
	{
		if (displayedMesh->faceVertices[i] == v1 || displayedMesh->faceVertices[i] == v2)
		{
			displayedMesh->faceVertices[i] = v1;//new_index;
		}
		// if(displayedMesh->faceVertices[i] >= v1)
		// {
		// 	displayedMesh->faceVertices[i]--;
		// }
		// if(displayedMesh->faceVertices[i] >= v2)
		// {
		// 	displayedMesh->faceVertices[i]--;
		// }
	}

	for(int i = 0; i < displayedMesh->faceVertices.size() / 3; i++){
		std::cout << displayedMesh->faceVertices[i*3 + 0] << " ";
		std::cout << displayedMesh->faceVertices[i*3 + 1] << " ";
		std::cout << displayedMesh->faceVertices[i*3 + 2] << std::endl;
	}
}